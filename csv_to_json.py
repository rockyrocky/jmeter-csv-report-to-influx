import csv
import datetime


def read_from_csv(csvfile):
    csv_file = open(csvfile, "r+")
    reader = csv.DictReader(csv_file)
    headers = reader.fieldnames
    return (headers, reader)


def count_lines_in_csv(csvfile):
    num_lines = sum(1 for line in open(csvfile))
    return num_lines


def get_row_from_csv(headers, row):
    print(headers)
    print(row)
    data = {}
    for header in headers:
        try:
            data[header] = float(row[header])
        except ValueError:
            data[header] = row[header]
    return data


def convert_to_influx_json(data, timestamp, tagkey, tagvalue):
    keys = [tagkey, ]
    values = [tagvalue, ]
    if "," in str(tagkey):
        keys = tagkey.split(",")
    if "," in str(tagvalue):
        values = tagvalue.split(",")
    tags = dict(zip(keys, values))
    for json_key in data:
        print(json_key)
        if not str(json_key).__eq__(timestamp):
            json_body = [
                {
                    "measurement": str(json_key),
                    "tags":
                        tags
                    ,
                    "time":
                        datetime.datetime.fromtimestamp(
                            int(data[timestamp])
                        ).strftime('%Y-%m-%d %H:%M:%S'),
                    "fields": {
                        "value": data[json_key]
                    }
                }
            ]
            print(json_body)
            yield json_body
