from influxdb import InfluxDBClient
import csv_to_json
import argparse
import threading
import multiprocessing


class Worker(threading.Thread):
    def __init__(self, headers, row, args, client):
        super().__init__()
        self.headers = headers
        self.row = row
        self.args = args
        self.client = client

    threadLimiter = threading.BoundedSemaphore(1)

    def run(self):
        self.threadLimiter.acquire()
        try:
            self.execute()
        finally:
            self.threadLimiter.release()

    def execute(self):
        data = csv_to_json.get_row_from_csv(self.headers, self.row)
        influx_data = csv_to_json.convert_to_influx_json(data, self.args.timestamp, self.args.tagkey,
                                                         self.args.tagvalue)
        for json in influx_data:
            write_to_database(self.client, json)


def arg_parser():
    parser = argparse.ArgumentParser(description="converts csv file to influx data points")
    parser.add_argument("-f", "--filename", type=str, required=True, help="source csv file")
    parser.add_argument("-t", "--timestamp", type=str, required=True, help="define timestamp column")
    parser.add_argument("-d", "--database", type=str, help="target database(creates if doesn't exist)")
    parser.add_argument("-tgk", "--tagkey", type=str, help="define tag key(comma separated if multiple)")
    parser.add_argument("-tgv", "--tagvalue", type=str, help="define tag value(comma separated if multiple)")
    args = parser.parse_args()
    return args


def connect_to_database(host="127.0.0.1", port=8086, database="testDB", username="root", password="root"):
    return InfluxDBClient(host=host, port=port, username=username, password=password, database=database)


def switch_database(client, args):
    if args.database is not None:
        client.create_database(args.database)
        client.switch_database(args.database)


def write_to_database(client, query):
    client.write_points(query)


def main():
    args = arg_parser()
    client = connect_to_database()
    switch_database(client, args)
    num_lines = csv_to_json.count_lines_in_csv(args.filename)
    headers, reader = csv_to_json.read_from_csv(args.filename)
    for x in range(num_lines - 1):
        for row in reader:
            Worker(headers, row, args, client).start()


if __name__ == '__main__':
    main()
